# php-env-file-reader

## Description

A simple class to read one or more dotenv files from various locations and loading them into the php environment.

TODO: Add instructions on env files.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Installation

```bash
composer require dolfen/env-file-reader
```

## Usage

Given this directory structure:

```
├── .env
├── index.php
├── composer.json
├── composer.lock
├── src
│   └── ExampleController.php
└── vendor
    ├── dolfen
    │   └── env-file-reader
    └── ...
```

We will be loading the environment variables in the index.php.

```php
<?php

require_once(__DIR__ . '/vendor/autoload.php');

use Dolfen\Environment;

Environment::readEnvFile(__DIR__ . '/.env');

$exampleController = new ExampleController();

...
```

Now you can access the variables set in the dotenv file using
[getenv()](https://www.php.net/manual/en/function.getenv.php).

## Contributing

I do not accept any modifications to the codebase by other developers right now, but feel free to raise an issue if you
encounter any bug or edge case that I did not catch.

## License

This project is licensed under MIT. See [license](https://gitlab.com/packagist-projects/php-env-file-reader/-/blob/main/LICENSE) for further information.
