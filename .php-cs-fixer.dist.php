<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('node_modules');

$phpCsFixer = new PhpCsFixer\Config();
$phpCsFixer->setFinder($finder);
/**
 * @see https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/master/doc/rules/index.rst
 */
$phpCsFixer->setRules([
    // rule lists
    '@PSR12' => true,
    '@PSR12:risky' => true,
    '@DoctrineAnnotation' => true,
    '@Symfony' => true,
    '@Symfony:risky' => true,
    '@PhpCsFixer' => true,
    '@PhpCsFixer:risky' => true,
    // specific rules
    'strict_param' => true,
    'array_syntax' => ['syntax' => 'short'],
    'global_namespace_import' => [
        'import_constants' => true,
        'import_functions' => true,
        'import_classes' => true,
    ],
    'declare_strict_types' => true,
    'strict_comparison' => true,
    'strict_param' => true,
]);

return $phpCsFixer;
