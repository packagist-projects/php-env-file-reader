<?php

declare(strict_types=1);

namespace Dolfen;

use Dolfen\Loaders\DotenvLoader;
use Exception;

/**
 * A simple class to read one or more .env files into the php environment variables.
 * This requires at least PHP 8.0.
 *
 * @version 0.1.1
 *
 * @since 0.1.0
 *
 * @author Tobias Dolfen <tobias@dolfen.me>
 */
final class Environment
{
    private function __construct()
    {
        // Make this private to throw errors when calling new on this static class.
    }

    /**
     * @throws Exception
     */
    public static function readEnvFile(string $path, bool $debug = false): void
    {
        try {
            DotenvLoader::readFile($path, $debug);
        } catch (Exception $e) {
            throw new Exception(sprintf('An error occurred while processing %s', $path), 4, $e);
        }
    }

    /**
     * @param string[] $paths
     *
     * @throws Exception
     */
    public static function readMultipleEnvFiles(array $paths, bool $debug = false): void
    {
        foreach ($paths as $path) {
            try {
                DotenvLoader::readFile($path, $debug);
            } catch (Exception $e) {
                throw new Exception(sprintf('An error occurred while processing %s', $path), 4, $e);
            }
        }
    }
}
