<?php

declare(strict_types=1);

namespace Dolfen\Loaders;

use function count;

use Exception;

/**
 * Class DotenvLoader.
 *
 * A simple loader reading .env files and loading them into the local environment via putenv().
 *
 * @version 0.1.0
 *
 * @since 0.1.0
 *
 * @author Tobias Dolfen <tobias@dolfen.me>
 */
final class DotenvLoader implements iLoader
{
    /**
     * @throws Exception
     */
    public static function readFile(string $path, bool $switch): void
    {
        $lines = file($path);

        if (!$lines) {
            throw new Exception(sprintf('No dotenv file found at location %s.', $path), 2);
        }

        foreach ($lines as $line) {
            $envLine = str_replace(['"', ' ', "\n"], '', $line);

            if (count(explode('=', $envLine)) < 2) {
                continue;
            }

            list($envKey, $envValue) = explode('=', $envLine);

            if (('' === $envKey || '' === $envValue) || (str_starts_with($envKey, '#') || str_starts_with($envValue, '#'))) {
                continue;
            }

            if (!putenv("{$envLine}") || empty($envKey)) {
                throw new Exception(sprintf('There was an error inserting the variable declaration %s into PHP\'s environment variables.', $envLine), 3);
            }

            $_ENV[$envKey] = $envValue;

            if ($switch) {
                self::printOut($envLine);
            }
        }
    }

    private static function printOut(mixed $out): void
    {
        echo '<pre>';
        var_dump($out);
        echo '</pre>';
    }
}
