<?php

declare(strict_types=1);

namespace Dolfen\Loaders;

/**
 * A simple abstraction layer to provide needed methods for more Loaders (planned in the future).
 *
 * @version 0.1.0
 *
 * @since 0.1.0
 *
 * @author Tobias Dolfen <tobias@dolfen.me>
 */
interface iLoader
{
    public static function readFile(string $path, bool $switch): void;
}
