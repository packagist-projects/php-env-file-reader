docker_run_php:= docker run --rm -it -v "$(shell pwd):/app" --workdir="/app" --entrypoint="" php:8.0-alpine
docker_run_composer:= docker run --rm -it -v "$(shell pwd):/app" --workdir="/app" --entrypoint="" jitesoft/composer:8.0

composer/install:
	$(docker_run_composer) composer install --prefer-dist --optimize-autoloader --no-interaction

composer/update:
	$(docker_run_composer) composer update

test: test/local/phpstan test/local/phpcs

test/local/phpstan:
	$(docker_run_php) vendor/bin/phpstan --configuration=phpstan.neon

test/local/phpcs:
	$(docker_run_php) vendor/bin/php-cs-fixer fix --diff --allow-risky=yes --dry-run

test/pipeline/phpstan:
	vendor/bin/phpstan --configuration=phpstan.neon

test/pipeline/phpcs:
	vendor/bin/php-cs-fixer fix --diff --allow-risky=yes --dry-run
